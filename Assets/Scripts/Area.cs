using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using YamlDotNet.Serialization;

[RequireComponent(typeof(AreaText))]
[RequireComponent(typeof(AreaHighlighter))]
public class Area : MonoBehaviour, GraphSerialisable
{
    public const float AreaYPosition = 0.01f;
    public const float BrushToolPosition = 0.02f;
    public const float PathYPosition = 0.03f;
    public const float AreaTextYPosition = 0.04f;
    private string areaName;
    private readonly List<Area> children = new List<Area>();

    private Color color;

    [NonSerialized] public UnityEvent colorChangedEvent = new UnityEvent();

    private readonly List<Connection> connections = new List<Connection>();
    private AreaHighlighter highlighter;
    private MeshCollider mc;
    private MeshFilter mf;

    private readonly Guid myGUID = Guid.NewGuid();
    private Color overrideColor;

    private Area parent;
    private AreaText text;
    private bool useOverrideColor;

    public string Name
    {
        get => areaName;
        set
        {
            areaName = value;
            gameObject.name = value;
        }
    }

    public Color Color
    {
        get => UseOverrideColor ? OverrideColor : color;
        set
        {
            color = value;
            colorChangedEvent.Invoke();
        }
    }

    public Color OverrideColor
    {
        get => parent == null ? overrideColor : parent.overrideColor;
        set => overrideColor = value;
    }

    public bool UseOverrideColor
    {
        get => parent == null ? useOverrideColor : parent.useOverrideColor;
        set
        {
            useOverrideColor = value;
            colorChangedEvent.Invoke();
        }
    }

    public BrushMesh BrushMesh { get; set; }

    private void Awake()
    {
        mf = GetComponent<MeshFilter>();
        mc = GetComponent<MeshCollider>();
        highlighter = GetComponent<AreaHighlighter>();
        text = GetComponent<AreaText>();
    }

    private void OnDestroy()
    {
        //destroy children areas as well
        foreach (Area child in children)
            if (child)
                Destroy(child.gameObject);
    }

    private void OnDrawGizmos()
    {
        BrushMesh.OnDrawDebug();
        //show bounds
        Gizmos.color = Color.cyan;
        Bounds b = GetBounds();
        Gizmos.DrawWireCube(b.center, b.extents * 2);
    }

    public Bounds GetBounds()
    {
        return BrushMesh.GetUnityMesh().bounds;
    }

    public bool PointInBounds(Vector3 point)
    {
        //check axis aligned bounding box for coarse intersection first
        Vector3 pointXZ = new Vector3(point.x, 0, point.z); //set y-coordinate to 0 because we are in local space

        return GetBounds().Contains(pointXZ);
    }

    public bool PointInArea(Vector3 point)
    {
        //check axis aligned bounding box for coarse intersection first
        Vector3 pointXZ = new Vector3(point.x, 0, point.z); //set y-coordinate to 0 because we are in local space
        if (!GetBounds().Contains(pointXZ)) return false;

        //point is in AABB so we need to check finer
        return BrushMesh.PointInMesh(new Vector2(point.x, point.z));
    }

    public void Merge(BrushMesh mesh)
    {
        BrushMesh.Merge(mesh);
        RedrawBrushMesh();
    }

    public void Subtract(Area area)
    {
        Subtract(area);
    }

    public void Subtract(BrushMesh mesh)
    {
        BrushMesh.Subtract(mesh);
        RedrawBrushMesh();
    }

    public void Intersect(Area area)
    {
        Intersect(area.BrushMesh);
    }

    public void Intersect(BrushMesh mesh)
    {
        BrushMesh.Intersect(mesh);
        RedrawBrushMesh();
    }

    public void FillHoles()
    {
        BrushMesh.FillHoles();
        RedrawBrushMesh();
    }

    public void Expand(float amount)
    {
        BrushMesh.MoveBoundariesAlongNormal(amount);
        BrushMesh.Recalculate();
    }

    public void Shrink(float amount)
    {
        BrushMesh.MoveBoundariesAlongNormal(-amount);
        BrushMesh.Recalculate();
    }

    public void RedrawBrushMesh()
    {
        if (BrushMesh.ScheduledForDeletion) return;

        mf.sharedMesh = BrushMesh.GetUnityMesh();
        mc.sharedMesh = BrushMesh.GetUnityMesh();
    }

    public bool ScheduledForDeletion()
    {
        return BrushMesh.ScheduledForDeletion;
    }

    public float[][] GetPoints()
    {
        List<Vector3> vertices3D = new List<Vector3>();
        GetComponent<MeshFilter>().sharedMesh.GetVertices(vertices3D);

        IEnumerable<float[]> floats = vertices3D.Select(vector3 => new[] {vector3.x, vector3.z});

        return floats.ToArray();
    }

    public void SetHighlight(bool value)
    {
        highlighter.SetHighlight(value);
    }

    public void SetPriority(bool value, Color priorityColor, Color normalColor)
    {
        //move area name to above area
        highlighter.SetPriority(value, priorityColor, normalColor);
        text.SetPriority(value);
    }

    public Area GetParent()
    {
        return parent;
    }

    public void SetParent(Area parent)
    {
        this.parent = parent;
    }

    public List<Area> GetChildren()
    {
        return children;
    }

    public bool IsChildless()
    {
        return children.Count == 0;
    }

    public void AddChild(Area child)
    {
        children.Add(child);
    }

    public void AddChildren(List<Area> children)
    {
        foreach (Area area in children) AddChild(area);
    }

    public List<Area> GetAncestors()
    {
        List<Area> result = new List<Area>();
        Area tempArea = parent;
        while (tempArea != null)
        {
            result.Add(tempArea);
            tempArea = tempArea.GetParent();
        }

        result.Reverse();

        return result;
    }

    public int GetTreeDepth()
    {
        return GetAncestors().Count;
    }

    public void AddConnection(Connection connection)
    {
        if (connection == null)
            return;

        //dont add duplicates
        if (connections.Contains(connection))
        {
            Debug.LogWarning("Tried adding connection as a duplicate.");
            return;
        }

        connections.Add(connection);
    }

    public List<Area> GetNeighbors(bool directOnly)
    {
        List<Area> result = new List<Area>();

        //list all connected areas
        foreach (Connection connection in connections)
        {
            //direct connections only
            if (directOnly && connection.CurrentArea != this)
                continue;

            if (connection.OtherConnection.CurrentArea != this) result.Add(connection.OtherConnection.CurrentArea);
        }

        //remove duplicates
        return result.Distinct().ToList();
    }

    public void RemoveConnection(Connection c)
    {
        if (c == null || !connections.Contains(c))
            return;

        connections.Remove(c);
    }

    public void RemoveAllConnections()
    {
        connections.Clear();
        foreach (Area child in children) child.RemoveAllConnections();
    }


    #region Serialisation

    public class AreaSave
    {
        public float b;
        public AreaSave[] children;
        public float g;
        public Guid ID;
        public string name;
        public float r;

        [YamlIgnore] public Mesh mesh { get; set; }
    }

    public AreaSave GetSaveData()
    {
        AreaSave[] childSaves = new AreaSave[children.Count];
        for (int i = 0; i < childSaves.Length; i++)
        {
            childSaves[i] = children[i].GetSaveData();
        }

        AreaSave result = new AreaSave
        {
            ID = myGUID,
            mesh = BrushMesh.GetUnityMesh(),
            name = areaName,
            r = color.r,
            b = color.b,
            g = color.g,
            children = childSaves
        };

        return result;
    }

    #endregion
}