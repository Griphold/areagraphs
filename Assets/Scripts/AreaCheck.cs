using UnityEngine;

public class AreaCheck : MonoBehaviour
{
    private AreaManager areaManager;

    private void Awake()
    {
        areaManager = FindObjectOfType<AreaManager>();
    }

    public Area GetCurrentArea()
    {
        return areaManager.GetAreaAt(transform.position);
    }
}