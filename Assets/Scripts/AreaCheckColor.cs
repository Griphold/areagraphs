using UnityEngine;

public class AreaCheckColor : AreaCheck
{
    private Material material;
    private Color originalColor;

    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<Renderer>().material;
        originalColor = material.GetColor("_BaseColor");
    }

    // Update is called once per frame
    void Update()
    {
        Area a = GetCurrentArea();
        Color myColor = a ? a.Color : originalColor;

        material.SetColor("_BaseColor", myColor);
    }
}