using UnityEngine;

[RequireComponent(typeof(Area))]
public class AreaHighlighter : MonoBehaviour
{
    private const float SelectedOpacity = 1f;
    private const float NotSelectedOpacity = 0.45f;
    private Material[] materials;
    private Area area;
    private bool priority = false;
    private bool highlighted = false;

    // Start is called before the first frame update
    void Awake()
    {
        area = GetComponent<Area>();
        materials = GetComponent<MeshRenderer>().materials;

        area.colorChangedEvent.AddListener(OnColorChanged);
    }

    private void Start()
    {
        SetHighlight(false);
    }

    public void OnColorChanged()
    {
        SetHighlight(highlighted);
    }

    public void SetHighlight(bool value)
    {
        highlighted = value;

        if (priority)
            return;

        Color c = area.Color;
        c.a = value ? SelectedOpacity : NotSelectedOpacity;
        materials[0].SetColor("MainColor", c);
    }

    public void SetPriority(bool value, Color priorityColor, Color normalColor)
    {
        Color c = value ? priorityColor : area.Color;

        if (value)
        {
            c.a = NotSelectedOpacity;
            materials[0].SetColor("MainColor", c);
            materials[0].renderQueue = 2989;
            materials[1].renderQueue = 2990;
        }
        else
        {
            c.a = NotSelectedOpacity;
            materials[0].SetColor("MainColor", c);
            materials[0].renderQueue = 3000;
            materials[1].renderQueue = 3001;
        }

        priority = value;
    }
}