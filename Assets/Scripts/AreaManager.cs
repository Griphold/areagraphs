using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class AreaManager : MonoBehaviour
{
    enum ColorMode
    {
        ShowColors,
        DoNotShowColors
    }

    [SerializeField] private GameObject NewAreaPrefab;

    [SerializeField] private ColorMode colorMode = ColorMode.ShowColors;
    [SerializeField] private Color MainContextColor;
    [SerializeField] private Color NormalColor;

    private List<Area> elderAreas = new List<Area>();

    private int nameCounter = 1;

    private Area context = null;

    private void AddArea(Area a)
    {
        a.SetParent(context);
        a.Name = GenerateName(a);

        GetAreas().Add(a);
    }

    private string GenerateName(Area newArea)
    {
        Area parent = newArea.GetParent();
        List<string> takenNames;
        string pattern;
        if (parent)
        {
            takenNames = parent.GetChildren().Select(c => c.Name).ToList(); //get all child names
            pattern = @".+-([0-9]+)\b";
        }
        else
        {
            takenNames = elderAreas.Select(e => e.Name).ToList();
            pattern = @"\b[Aa]rea ([0-9]+)\b";
        }

        //match e.g. area-1 OR area-213-20

        Regex rgx = new Regex(pattern);
        List<int> numbersTaken = new List<int>();

        //go through all names
        foreach (string childName in takenNames)
        {
            //look for names with a hyphen and number behind 
            Match match = rgx.Match(childName);
            if (match.Success)
            {
                //number group
                Group group = match.Groups[1];
                if (group.Success)
                {
                    CaptureCollection captures = group.Captures;
                    Capture c = captures[0];
                    //try to parse the integer
                    int integer;
                    if (int.TryParse(c.ToString(), out integer))
                    {
                        numbersTaken.Add(integer);
                    }
                }
            }
        }

        //look for lowest number not taken
        numbersTaken.Sort();
        string result = "";
        if (numbersTaken.Count > 0)
        {
            foreach (int integer in numbersTaken)
            {
                if (!numbersTaken.Contains(integer + 1))
                {
                    //if we have a parent add to that name
                    if (parent)
                        result = parent.Name + "-" + (integer + 1);
                    //if we are an elder area count up with "Area x"
                    else
                        result = "Area " + (integer + 1);
                }
            }
        }
        else
        {
            //if we have a parent the first area is named "ParentName-1"
            if (parent)
                result = parent.Name + "-" + "1";
            //if there is no parent the first area is named "Area1"
            else
                result = "Area 1";
        }


        return result;
    }

    public HashSet<Area> GetElderAreas()
    {
        return new HashSet<Area>(elderAreas);
    }

    private List<Area> GetAreas()
    {
        if (context == null)
        {
            return elderAreas;
        }
        else
        {
            return context.GetChildren();
        }
    }

    public void ToggleColorMode()
    {
        colorMode = colorMode == ColorMode.ShowColors ? ColorMode.DoNotShowColors : ColorMode.ShowColors;

        switch (colorMode)
        {
            case ColorMode.ShowColors:

                elderAreas.ForEach(x => x.UseOverrideColor = false);
                elderAreas.ForEach(x => x.OverrideColor = NormalColor);
                break;

            case ColorMode.DoNotShowColors:
                elderAreas.ForEach(x => x.UseOverrideColor = true);
                elderAreas.ForEach(x => x.OverrideColor = NormalColor);
                break;
        }
    }

    //Will set y coordinate to 0
    public Area GetAreaAt(Vector3 pointOnPlane, bool useContext = true)
    {
        Area resultArea = null;
        if (useContext)
        {
            //check point in 2d polygon intersection for each mesh
            foreach (Area area in GetAreas())
            {
                if (area.PointInArea(pointOnPlane))
                {
                    resultArea = area;
                    break;
                }
            }
        }
        else
        {
            List<Area> areasToCheck = elderAreas;
            bool changed;
            do
            {
                changed = false;
                //check point in 2d polygon intersection for each mesh
                foreach (Area area in areasToCheck)
                {
                    if (area.PointInArea(pointOnPlane))
                    {
                        resultArea = area;
                        changed = true;
                        break;
                    }
                }

                if (changed)
                {
                    areasToCheck = resultArea.GetChildren();
                }
            } while (changed && areasToCheck.Count > 0);
        }

        return resultArea;
    }

    public void SetContext(Area newContext, bool forceUpdate = false)
    {
        //did context change at all
        if (!forceUpdate && context == newContext)
            return;

        //is context null? yes -> enable elder areas, disable all their children
        List<Area> toDisable;
        if (context == null)
        {
            toDisable = elderAreas;
        }
        else
        {
            toDisable = context.GetChildren();

            //disable old context
            context.gameObject.SetActive(false);
            context.SetPriority(false, MainContextColor, NormalColor);
        }

        //disable old children
        foreach (Area child in toDisable)
        {
            child.gameObject.SetActive(false);
        }

        List<Area> toEnable;
        if (newContext == null)
        {
            toEnable = elderAreas;
        }
        else
        {
            //enable context so we can see it
            newContext.gameObject.SetActive(true);
            newContext.SetPriority(true, MainContextColor, NormalColor);

            toEnable = newContext.GetChildren();
        }

        context = newContext;

        //enable new context children and clip each to the context as it might have updated
        foreach (Area area in toEnable)
        {
            area.gameObject.SetActive(true);
            ClipCurrentContext(area);
        }

        CheckFullyDeletedAreas();
    }

    public void MoveContextUp()
    {
        if (context == null)
            return;

        SetContext(context.GetParent());
    }

    public Area StartNewArea(float brushDiameter, Vector2 position, int areaResolution)
    {
        //create new gameobject to store new area
        //these are always at "0,0,0" so we can merge them better
        //+ 0.1f on y-axis so no z-fighting occurs
        GameObject areaGameObject =
            Instantiate(NewAreaPrefab, new Vector3(0, Area.AreaYPosition, 0), Quaternion.identity);

        //create mesh and add it to the new game object
        BrushMesh b = Mesh2DFactory.CreateCircleMesh(brushDiameter, areaResolution, position);
        MeshFilter mf = areaGameObject.GetComponent<MeshFilter>();
        mf.mesh = b.GetUnityMesh();

        //add collider
        MeshCollider mc = areaGameObject.GetComponent<MeshCollider>();
        mc.sharedMesh = b.GetUnityMesh();

        //add the area to the areamanager
        Area newArea = areaGameObject.GetComponent<Area>();
        newArea.BrushMesh = b;

        //assign color
        Color c = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 0.58f, 0.75f);
        newArea.Color = c;
        newArea.OverrideColor = NormalColor;
        newArea.UseOverrideColor = colorMode == ColorMode.DoNotShowColors;

        AddArea(newArea);

        return newArea;
    }

    public void DeleteAreaAndChildren(Area a)
    {
        a.BrushMesh.ScheduledForDeletion = true;
        CheckFullyDeletedAreas();
    }

    public void SubtractFromAll(Area a)
    {
        //subtract only from areas in vicinity
        //do a simple AABB check before
        foreach (Area area in GetAreas())
        {
            //remove area from all other meshes
            if (!area.Equals(a))
                area.Subtract(a.BrushMesh);
        }

        CheckFullyDeletedAreas();
    }

    public void SubtractFromAll(Bounds bounds, BrushMesh mesh)
    {
        SubtractFromAll(bounds, mesh, null);
    }

    public void SubtractFromAll(Bounds bounds, BrushMesh mesh, params Area[] exclude)
    {
        //check AABB distance
        HashSet<Area> areasInRange = AreasInVicinity(bounds);
        if (areasInRange.Count == 0)
            return;

        if (exclude == null || exclude.Length <= 0)
        {
            //do a simple AABB check before
            foreach (Area area in areasInRange)
            {
                area.Subtract(mesh);
            }
        }
        else
        {
            foreach (Area area in areasInRange)
            {
                //exlude areas from the exclude list
                if (!exclude.Contains(area))
                    area.Subtract(mesh);
            }
        }

        CheckFullyDeletedAreas();
    }

    public void ClipCurrentContext(Area a)
    {
        if (context != null)
            a.Intersect(context);
    }

    public void InitializeFromSaveData(List<Area.AreaSave> saveData, Dictionary<string, Mesh> meshes)
    {
        Clear();

        List<Area> elderAreas = InitializeAreasRecursive(saveData.ToArray(), meshes, null);

        this.elderAreas = elderAreas;

        SetContext(null, true);
    }

    private List<Area> InitializeAreasRecursive(Area.AreaSave[] saveData, Dictionary<string, Mesh> meshes, Area parent)
    {
        List<Area> result = new List<Area>(saveData.Length);

        foreach (Area.AreaSave areaSave in saveData)
        {
            string name = areaSave.name;
            Color color = new Color(areaSave.r, areaSave.g, areaSave.b);
            Mesh mesh = meshes[areaSave.ID.ToString()];
            Area newArea = InitializeAreaFromMesh(name, color, mesh);
            newArea.SetParent(parent);
            newArea.gameObject.SetActive(false);

            //children
            if (areaSave.children.Length > 0)
            {
                List<Area> children = InitializeAreasRecursive(areaSave.children, meshes, newArea);
                newArea.AddChildren(children);
            }

            result.Add(newArea);
        }

        return result;
    }

    private Area InitializeAreaFromMesh(string name, Color color, Mesh m)
    {
        GameObject areaGameObject =
            Instantiate(NewAreaPrefab, new Vector3(0, Area.AreaYPosition, 0), Quaternion.identity);
        Area newArea = areaGameObject.GetComponent<Area>();
        newArea.Name = name;
        newArea.Color = color;

        BrushMesh b = new BrushMesh(m);
        newArea.BrushMesh = b;

        newArea.RedrawBrushMesh();
        return newArea;
    }

    private HashSet<Area> AreasInVicinity(Bounds bounds)
    {
        HashSet<Area> areasInRange = new HashSet<Area>();

        foreach (Area area in GetAreas())
        {
            Bounds areaBounds = area.GetBounds();

            if (bounds.Intersects(areaBounds))
                areasInRange.Add(area);
        }

        return areasInRange;
    }

    private void CheckFullyDeletedAreas()
    {
        //check fully deleted areas
        List<Area> contextAreas = GetAreas();
        for (int i = contextAreas.Count - 1; i >= 0; i--)
        {
            Area a = contextAreas[i];
            if (a.ScheduledForDeletion())
            {
                contextAreas.RemoveAt(i);
                Destroy(a.gameObject);
            }
        }
    }

    private void Clear()
    {
        List<Area> contextAreas = elderAreas;
        for (int i = 0; i < contextAreas.Count; i++)
        {
            Area a = contextAreas[i];
            DestroyImmediate(a.gameObject);
        }

        contextAreas.Clear();

        context = null;
    }

    public List<Area.AreaSave> GetSaveData()
    {
        List<Area.AreaSave> areaSaves = new List<Area.AreaSave>(elderAreas.Count);

        foreach (Area area in elderAreas)
        {
            areaSaves.Add(area.GetSaveData());
        }

        return areaSaves;
    }
}