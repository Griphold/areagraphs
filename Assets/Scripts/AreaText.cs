using TMPro;
using UnityEngine;

[RequireComponent(typeof(Area))]
public class AreaText : MonoBehaviour
{
    [SerializeField] private float LerpSpeed = 100;

    [SerializeField] private RectTransform textRect;

    [SerializeField] private TMP_InputField inputField;

    private Area area;
    private bool isPriority = false;

    private void Start()
    {
        area = GetComponent<Area>();

        textRect.position = area.GetBounds().center;

        inputField.text = area.Name;
        inputField.onSubmit.AddListener(OnSubmit);

        inputField.onSelect.AddListener(OnSelect);
        inputField.onDeselect.AddListener(OnDeselect);
        inputField.onEndEdit.AddListener(OnDeselect);
    }

    void Update()
    {
        textRect.position = GetNewPosition();

        Vector3 boundsExtents = area.GetBounds().extents;
        /*textObject.
        textObject.rect = new Rect(boundsExtents.x, boundsExtents.z);
        textObject.rect.width = boundsExtents.x;
        textObject.rect.height = boundsExtents.z;*/
    }

    private Vector3 GetNewPosition()
    {
        if (isPriority)
        {
            Bounds bounds = area.GetBounds();
            float zPosition = bounds.max.z + ((RectTransform) inputField.transform).rect.height * 1.1f;

            Vector3 targetPosition = new Vector3(bounds.center.x, Area.AreaTextYPosition, zPosition);
            return Vector3.Lerp(textRect.position, targetPosition, Time.deltaTime * LerpSpeed);
        }
        else
        {
            Vector3 boundsCenter = area.GetBounds().center;
            boundsCenter = new Vector3(boundsCenter.x, Area.AreaTextYPosition, boundsCenter.z);
            return Vector3.Lerp(textRect.position, boundsCenter, Time.deltaTime * LerpSpeed);
        }
    }

    private void OnSubmit(string s)
    {
        area.Name = s;
    }

    public void SetPriority(bool value)
    {
        isPriority = value;
    }

    private void OnSelect(string s)
    {
        PlayerInput.Instance.IsTyping = true;
    }

    private void OnDeselect(string s)
    {
        PlayerInput.Instance.IsTyping = false;
    }
}