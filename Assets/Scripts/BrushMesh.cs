using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;

public class BrushMesh
{
    private const float SCALE_FACTOR = 1000f;

    private Vector3[] vertices3D;
    private List<EdgeLoop> boundaryEdges;
    private List<EdgeLoop> holes;
    private Mesh unityMesh;
    public bool ScheduledForDeletion { get; set; } = false;

    public List<EdgeLoop> savedEdgeLoops = new List<EdgeLoop>();

    public BrushMesh(Mesh mesh)
    {
        SetUnityMesh(mesh);
    }

    public BrushMesh(List<Vector2> edgePoints, Vector3[] vertices3D, int[] triangles)
    {
        this.vertices3D = vertices3D;

        // Create the mesh
        Mesh msh = new Mesh();
        msh.vertices = vertices3D;
        msh.triangles = triangles;
        SetUnityMesh(msh);
    }

    public Mesh GetUnityMesh()
    {
        return unityMesh;
    }

    public void SetUnityMesh(Mesh m)
    {
        vertices3D = m.vertices;

        //save it back in this mesh
        if (unityMesh != null)
        {
            unityMesh.Clear();
            unityMesh.SetVertices(m.vertices, 0, m.vertexCount);
            unityMesh.SetTriangles(m.triangles, 0);
        }
        else
        {
            Mesh newMesh = new Mesh();
            unityMesh = newMesh;
            unityMesh.SetVertices(m.vertices, 0, m.vertexCount);
            unityMesh.SetTriangles(m.triangles, 0);
        }

        unityMesh.RecalculateNormals();
        RecalculateBoundaryEdges();
        RecalculateVertexUVs();
    }

    public List<EdgeLoop> GetBoundaryEdges()
    {
        return boundaryEdges;
    }

    public Vector3[] GetLocalVertexPositions()
    {
        List<Vector3> localVertices = new List<Vector3>();
        unityMesh.GetVertices(localVertices);
        return localVertices.ToArray();
    }

    public void Recalculate()
    {
        Mesh m = Mesh2DFactory.CreateMeshFromEdgeVertices(GetLocalVertexPositions().ToArray(), boundaryEdges, holes);
        SetUnityMesh(m);
    }

    public void Merge(BrushMesh other)
    {
        ClipOtherMesh(other, ClipType.ctUnion);
    }

    public void Subtract(BrushMesh other)
    {
        ClipOtherMesh(other, ClipType.ctDifference);
    }

    public void Intersect(BrushMesh other)
    {
        ClipOtherMesh(other, ClipType.ctIntersection);
    }

    public bool PointInMesh(Vector2 point)
    {
        //shoot a ray to +x and intersect it with all boundary edges. count the number of intersects:
        //- point is inside if odd number of intersects
        //- point is on outside if even number of intersects
        //aggregate all boundary edge loops and holes
        List<Edge> edges = new List<Edge>();
        foreach (EdgeLoop edgeLoop in GetBoundaryEdges())
        {
            edges.AddRange(edgeLoop);
        }

        foreach (EdgeLoop edgeLoop in holes)
        {
            edges.AddRange(edgeLoop);
        }

        int numOfIntersections = Intersections(point, edges.ToArray(), GetLocalVertexPositions());

        return numOfIntersections % 2 == 1;
    }

    /// <summary>
    /// Removes all holes from mesh
    /// </summary>
    public void FillHoles()
    {
        Mesh m = Mesh2DFactory.CreateMeshFromEdgeVertices(GetLocalVertexPositions().ToArray(), GetBoundaryEdges(),
            null);
        SetUnityMesh(m);
    }

    /// <summary>
    /// Moves each edge vertex along normal to expand the area.
    /// </summary>
    public void MoveBoundariesAlongNormal(float amount)
    {
        Vector3[] vertexPositions = GetLocalVertexPositions();

        Dictionary<int, Vector3> EdgeVertexNormals = CalculateEdgeNormals();

        //move each edge vertex along normal
        foreach (KeyValuePair<int, Vector3> keyValuePair in EdgeVertexNormals)
        {
            Vector3 oldPosition = vertexPositions[keyValuePair.Key];
            Vector3 newPosition = oldPosition + amount * keyValuePair.Value;
            vertexPositions[keyValuePair.Key] = newPosition;
        }

        Mesh m = GetUnityMesh();
        vertices3D = vertexPositions;
        m.SetVertices(vertexPositions);
        m.RecalculateBounds();
    }

    private void ClipOtherMesh(BrushMesh other, ClipType clipType)
    {
        //convert boundary list to scaled int list
        List<List<IntPoint>> originalPaths = ConvertToPaths(DeepCopyBoundaryEdges());
        //add holes
        List<List<IntPoint>> originalHoles = ConvertToPaths(DeepCopyHoleEdges());
        List<List<IntPoint>> originalWithHoles = Clip(originalPaths, originalHoles, ClipType.ctDifference);

        //convert other mesh to scaled int list
        List<List<IntPoint>> otherPaths = other.ConvertToPaths(other.DeepCopyBoundaryEdges());
        //add holes to other
        List<List<IntPoint>> otherHoles = other.ConvertToPaths(other.DeepCopyHoleEdges());
        List<List<IntPoint>> otherWithHoles = Clip(otherPaths, otherHoles, ClipType.ctDifference);

        //clip the geometry and get result
        //List<List<IntPoint>> intersection = Clip(originalWithHoles, otherWithHoles, ClipType.ctIntersection);
        List<List<IntPoint>> result = Clip(originalWithHoles, otherWithHoles, clipType);

        //if it didnt change no need to triangulate again
        /*if(intersection.Count == 0)
            return;*/

        /* int pointCountBefore = 0;
        foreach (List<IntPoint> intList in originalWithHoles)
        {
            pointCountBefore += intList.Count;
        }
        
        int pointCountAfter = 0;
        foreach (List<IntPoint> intList in difference)
        {
            pointCountAfter += intList.Count;
        }

        if (pointCountBefore == pointCountAfter)
        {
            return;
        }*/

        //if all point are clipped away this is an empty mesh -> delete
        if (result.Count == 0)
        {
            ScheduledForDeletion = true;
            return;
        }

        //convert new merged paths to edge loops
        List<Vector3> vertexPositions;
        List<EdgeLoop> edgeLoops = ConvertToEdgeLoops(result, out vertexPositions);

        //find holes and boundary edges
        List<EdgeLoop> boundaryLoops;
        List<EdgeLoop> holeLoops;
        ClassifyEdgeLoops(edgeLoops, vertexPositions.ToArray(), out boundaryLoops, out holeLoops);

        Mesh m = Mesh2DFactory.CreateMeshFromEdgeVertices(vertexPositions.ToArray(), boundaryLoops, holeLoops);
        SetUnityMesh(m);
    }

    private Dictionary<int, Vector3> CalculateEdgeNormals()
    {
        Vector3[] vertexPositions = GetLocalVertexPositions();

        //Calculate normals
        Dictionary<int, Vector3> EdgeVertexNormals = new Dictionary<int, Vector3>();
        List<EdgeLoop> allEdgeLoops = new List<EdgeLoop>(boundaryEdges);
        allEdgeLoops.AddRange(holes);
        foreach (EdgeLoop edgeLoop in allEdgeLoops)
        {
            for (int i = 0; i < edgeLoop.Count; i++)
            {
                Edge currentEdge = edgeLoop[i];
                Edge nextEdge = edgeLoop[(i + 1) % edgeLoop.Count];

                Vector3 left = vertexPositions[currentEdge.P1];
                Vector3 middle = vertexPositions[currentEdge.P2]; //currentEdge.P2 = nextEdge.P1
                Vector3 right = vertexPositions[nextEdge.P2];

                //Directions of each edge
                Vector3 leftEdge3D = middle - left;
                Vector3 rightEdge3D = right - middle;

                //Normals of each edge
                Vector3 leftNormal = Quaternion.AngleAxis(90, Vector3.down) * leftEdge3D;
                Vector3 rightNormal = Quaternion.AngleAxis(90, Vector3.down) * rightEdge3D;

                Vector3 normal = ((leftNormal + rightNormal) / 2f).normalized;

                if (!EdgeVertexNormals.ContainsKey(currentEdge.P2))
                    EdgeVertexNormals.Add(currentEdge.P2, normal);
            }
        }

        return EdgeVertexNormals;
    }

    /// <summary>
    /// Shoots a ray from point towards X+
    /// </summary>
    /// <param name="point">Point of origin</param>
    /// <param name="edges">Edges with 2 Vertex IDs to test against</param>
    /// <param name="vertexPositions">Local Space positions of vertices</param>
    /// <returns>Number of Intersections between the 2D ray and edges</returns>
    private int Intersections(Vector2 point, Edge[] edges, Vector3[] vertexPositions)
    {
        int numOfIntersections = 0;
        foreach (Edge edge in edges)
        {
            //is this edge intersecting the ray (x-z-Plane)?
            Vector3 p1 = vertexPositions[edge.P1];
            Vector3 p2 = vertexPositions[edge.P2];

            //is the point to the +x of the point?
            if (p1.x >= point.x || p2.x >= point.x)
            {
                // are the two points of the edge above and below the point to check? (or vice versa?)
                //if yes then we have an intersection
                if ((p1.z < point.y && p2.z >= point.y) || (p2.z < point.y && p1.z >= point.y))
                {
                    numOfIntersections++;
                }
            }
        }

        return numOfIntersections;
    }

    private void RecalculateBoundaryEdges()
    {
        List<Edge> allBoundaryEdges = new List<Edge>();

        //Get all triangles
        List<int> triangleList = new List<int>();
        unityMesh.GetTriangles(triangleList, 0);

        //for each triangle add 3 edges to the list of all edges
        List<Edge> allEdges = new List<Edge>();
        for (int i = 0; i < triangleList.Count; i = i + 3)
        {
            int p0 = triangleList[i];
            int p1 = triangleList[i + 1];
            int p2 = triangleList[i + 2];
            allEdges.Add(new Edge(p0, p1));
            allEdges.Add(new Edge(p1, p2));
            allEdges.Add(new Edge(p2, p0));
        }

        //filter out non boundary edges
        //count number of occurences of each edges
        Dictionary<Edge, int> mapOfOccurences = new Dictionary<Edge, int>(new EdgeEqualityComparer());
        foreach (Edge edge in allEdges)
        {
            if (mapOfOccurences.ContainsKey(edge))
                mapOfOccurences[edge]++;
            else
                mapOfOccurences.Add(edge, 1);
        }

        //it is only a boundary edge if the number of occurrences is 1
        foreach (KeyValuePair<Edge, int> occurence in mapOfOccurences)
        {
            if (occurence.Value == 1)
                allBoundaryEdges.Add(occurence.Key);
        }


        //find all boundary edges
        //start at one edge and follow it around, if there are any edges left we take one of the remaining ones and do another loop
        List<EdgeLoop> boundaryEdgeLoops = new List<EdgeLoop>();
        do
        {
            EdgeLoop onePath = new EdgeLoop();
            int loopStart = 0;
            int loopEnd = 0;
            do
            {
                //find next edge
                for (int i = 0; i < allBoundaryEdges.Count; i++)
                {
                    Edge edge = allBoundaryEdges[i];
                    bool found = false;
                    //if we are at the start of the loop just take first edge
                    if (onePath.Count == 0)
                    {
                        loopStart = edge.P1;
                        loopEnd = edge.P2;
                        onePath.Add(new Edge(edge.P1, edge.P2));
                        found = true;
                    }
                    else if (edge.P1 == loopEnd)
                    {
                        loopEnd = edge.P2;
                        onePath.Add(new Edge(edge.P1, edge.P2));
                        found = true;
                    }
                    else if (edge.P2 == loopEnd)
                    {
                        loopEnd = edge.P1;
                        onePath.Add(new Edge(edge.P2, edge.P1));
                        found = true;
                    }

                    if (found)
                    {
                        allBoundaryEdges.RemoveAt(i);
                        break;
                    }
                }
            } while (loopStart != loopEnd);

            if (onePath.Count > 1)
                boundaryEdgeLoops.Add(onePath);
        } while (allBoundaryEdges.Count > 0);

        //save edge loops for debug purposes
        savedEdgeLoops = boundaryEdgeLoops;

        //now we have lists of edge loops that are potentially the enclosing boundaries
        Vector3[] localVertexPositions = GetLocalVertexPositions();
        List<EdgeLoop> enclosingLoops;
        List<EdgeLoop> holeLoops;

        ClassifyEdgeLoops(boundaryEdgeLoops, localVertexPositions, out enclosingLoops, out holeLoops);

        boundaryEdges = enclosingLoops;
        holes = holeLoops;
    }

    private void RecalculateVertexUVs()
    {
        Vector3[] vertexPositions = GetLocalVertexPositions();
        Dictionary<int, Vector3> EdgeVertexNormals = CalculateEdgeNormals();

        //set outline normals to outward except non-boundary ones
        List<Vector3> outlineNormals = new List<Vector3>(vertexPositions);
        for (int i = 0; i < outlineNormals.Count; i++)
        {
            if (EdgeVertexNormals.ContainsKey(i))
            {
                outlineNormals[i] = EdgeVertexNormals[i];
            }
            else
            {
                outlineNormals[i] = Vector3.up;
            }
        }

        unityMesh.SetUVs(3, outlineNormals);
    }

    private void ClassifyEdgeLoops(List<EdgeLoop> input, Vector3[] localVertexPositions,
        out List<EdgeLoop> enclosingLoops, out List<EdgeLoop> holeLoops)
    {
        enclosingLoops = new List<EdgeLoop>();
        holeLoops = new List<EdgeLoop>();

        //if there is only one edge loop we are already done
        if (input.Count == 1)
        {
            enclosingLoops.Add(input[0]);
            return;
        }

        //for each edge loop
        //shoot a ray along X+ (ignoring ones own boundary edges): if we hit nothing or an even amount of edges then its the boundary, if we hit an odd amount of edges this edge loop is a hole
        //back to front because we are removing an element with each iteration
        for (int currentLoop = input.Count - 1; currentLoop >= 0; currentLoop--)
        {
            //get point
            EdgeLoop loop = input[currentLoop];
            Edge firstEdge = loop[0];
            int firstIndex = firstEdge.P1;
            Vector3 firstPosition = localVertexPositions[firstIndex];

            //get edges (ignore current)
            List<Edge> intersectEdges = new List<Edge>();
            for (int i = 0; i < input.Count; i++)
            {
                //ignore my own edges
                if (i != currentLoop)
                {
                    intersectEdges.AddRange(input[i]);
                }
            }


            //classify as boundary or hole
            int intersections = Intersections(new Vector2(firstPosition.x, firstPosition.z), intersectEdges.ToArray(),
                localVertexPositions);
            if (intersections % 2 == 0)
            {
                enclosingLoops.Add(loop);
            }
            else
            {
                holeLoops.Add(loop);
            }

            //do until all edge loops are classified
        }
    }

    private Vector3 CalculateBoundaryNormals(Edge[] edges)
    {
        Vector3 result = Vector3.zero;

        foreach (Edge edge in edges)
        {
            Vector3 positionP1 = vertices3D[edge.P1];
            Vector3 positionP2 = vertices3D[edge.P2];
            result += Quaternion.AngleAxis(90, Vector3.down) * (positionP2 - positionP1);
        }

        return (result / edges.Length).normalized;
    }

    private List<EdgeLoop> DeepCopyBoundaryEdges()
    {
        List<EdgeLoop> result = new List<EdgeLoop>();
        foreach (EdgeLoop boundaryEdge in GetBoundaryEdges())
        {
            EdgeLoop edgeLoop = new EdgeLoop();
            foreach (Edge edge in boundaryEdge)
            {
                edgeLoop.Add(new Edge(edge.P1, edge.P2));
            }

            result.Add(edgeLoop);
        }

        return result;
    }

    private List<EdgeLoop> DeepCopyHoleEdges()
    {
        List<EdgeLoop> result = new List<EdgeLoop>();
        foreach (EdgeLoop boundaryEdge in holes)
        {
            EdgeLoop edgeLoop = new EdgeLoop();
            foreach (Edge edge in boundaryEdge)
            {
                edgeLoop.Add(new Edge(edge.P1, edge.P2));
            }

            result.Add(edgeLoop);
        }

        return result;
    }

    //convert boundary edges to list of vertex positions grouped by connected mesh
    //assume boundary edges connect to full loops
    private List<List<IntPoint>> ConvertToPaths(List<EdgeLoop> edgeLoops)
    {
        List<List<IntPoint>> intPoints = new List<List<IntPoint>>();

        //go through each edge loop
        foreach (EdgeLoop edgeLoop in edgeLoops)
        {
            List<IntPoint> intList = new List<IntPoint>(edgeLoop.Count);
            //convert edge loops to index lists with no duplicate IDs
            List<int> indexList = edgeLoop.AsIndexList();

            //go through each index, get the vertex position and scale it up
            foreach (int id in indexList)
            {
                Vector3 position = vertices3D[id];

                //x = x, z = y
                int scaledUpValueX = (int) (position.x * SCALE_FACTOR);
                int scaledUpValueY = (int) (position.z * SCALE_FACTOR);

                intList.Add(new IntPoint(scaledUpValueX, scaledUpValueY));
            }

            intPoints.Add(intList);
        }


        return intPoints;
    }

    private List<EdgeLoop> ConvertToEdgeLoops(List<List<IntPoint>> input, out List<Vector3> vertexPositions)
    {
        List<EdgeLoop> result = new List<EdgeLoop>(input.Count);
        vertexPositions = new List<Vector3>();
        int index = 0;
        foreach (List<IntPoint> intPoints in input)
        {
            EdgeLoop edgeLoop = new EdgeLoop();
            for (int i = 0; i < intPoints.Count; i++)
            {
                IntPoint intPoint = intPoints[i];

                float scaledDownValueX = intPoint.X / SCALE_FACTOR;
                float scaledDownValueY = intPoint.Y / SCALE_FACTOR;

                vertexPositions.Add(new Vector3(scaledDownValueX, 0, scaledDownValueY));
                if (i != intPoints.Count - 1)
                {
                    edgeLoop.Add(new Edge(index++, index)); //count up for each next vertex
                }
                else
                {
                    edgeLoop.Add(new Edge(index, edgeLoop[0].P1));
                    index++;
                }
            }

            result.Add(edgeLoop);
        }

        return result;
    }

    private List<List<IntPoint>> Clip(List<List<IntPoint>> original, List<List<IntPoint>> other, ClipType type)
    {
        Clipper c = new Clipper();

        //add original polygons
        c.AddPaths(original, PolyType.ptSubject, true);
        //add clip
        c.AddPaths(other, PolyType.ptClip, true);

        //produce solution
        List<List<IntPoint>> solution = new List<List<IntPoint>>();
        c.Execute(type, solution);

        return solution;
    }

    public void OnDrawDebug()
    {
        if (savedEdgeLoops.Count <= 0)
            return;

        Vector3[] vertexPositions = GetLocalVertexPositions();
        int numOfColors = 10;
        Color[] colors = new Color[numOfColors];
        for (int i = 0; i < numOfColors; i++)
        {
            colors[i] = Color.HSVToRGB(1 / (float) (numOfColors - 1) * i, 1, 1);
            //colors[i] = Color.cyan;
        }

        int index = 0;
        foreach (EdgeLoop edgeLoop in savedEdgeLoops)
        {
            for (int i = 0; i < edgeLoop.Count; i++)
            {
                Vector3 p0 = vertexPositions[edgeLoop[i].P1];
                Vector3 p1 = vertexPositions[edgeLoop[i].P2];
                Debug.DrawLine(p0, p1, colors[index]);
            }

            index++;
        }
    }
}