using UnityEngine;

public class ChangeContext : MonoBehaviour
{
    [SerializeField] private AreaManager areaManager;

    private KeyCode HierarchyDownCode = KeyCode.PageDown; //page down to go into children
    private KeyCode HierarchyUpCode = KeyCode.PageUp; //page up to go up in hierarchy

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Area hoveredArea = areaManager.GetAreaAt(transform.position);

        if (PlayerInput.Instance.IsTyping)
            return;

        //go down hierarchy
        if (Input.GetKeyDown(HierarchyDownCode))
        {
            OnHierachyDown(hoveredArea);
        }
        //go up hierarchy
        else if (Input.GetKeyDown(HierarchyUpCode))
        {
            OnHierarchyUp();
        }
    }


    private void OnHierachyDown(Area a)
    {
        if (a == null)
            return;

        areaManager.SetContext(a);
    }

    private void OnHierarchyUp()
    {
        areaManager.MoveContextUp();
    }
}