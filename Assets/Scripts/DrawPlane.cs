using UnityEngine;

public class DrawPlane : MonoBehaviour
{
    [SerializeField] private DrawTool brush;
    [SerializeField] private ConnectorTool connector;

    // Update is called once per frame
    void Update()
    {
        Vector3 hitpoint = GetHitpointOnPlane();

        if (hitpoint != Vector3.zero)
        {
            brush.SetTargetPosition(hitpoint);
            connector.SetTargetPosition(hitpoint);
        }
    }

    private Vector3 GetHitpointOnPlane()
    {
        //make sure plane is rotated correctly
        Plane plane = new Plane(transform.up, transform.position);

        //fire ray from camera onto virtual plane
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float distance = 0;

        if (plane.Raycast(ray, out distance))
        {
            return ray.GetPoint(distance);
        }

        return Vector3.zero;
    }
}