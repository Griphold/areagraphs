using UnityEngine;
using UnityEngine.EventSystems;

public class DrawTool : MonoBehaviour, IScrollHandler, IDragHandler, IEndDragHandler, IPointerDownHandler
{
    public AreaManager areaManager;

    private KeyCode FillKeyCode = KeyCode.F; //F to fill
    private KeyCode ExpandKeyCode = KeyCode.E; //E to expand
    private KeyCode ShrinkKeyCode = KeyCode.S; //S to shrink
    private KeyCode DeleteKeyCode = KeyCode.Delete; //Delete to delete

    private MeshFilter mf;

    [SerializeField] private int AreaResolution = 20; //How many vertices does the brush itself have
    [SerializeField] private float MinimumBrushDiameter = 5;
    [SerializeField] private float MaximumBrushDiameter = 100;
    [SerializeField] private float BrushStartDiameter = 75;

    [SerializeField] private float scalingPerScroll = 5f; //how much should the diameter change per second of scrolling
    [SerializeField] private float ScalingLerpSpeed = 5f; //how fast does the size change animate
    [SerializeField] private float ExpandFactor = 40f; //How fast does the hovered area expand/shrink


    private float targetDiameter;
    private Vector3 targetPosition;
    private Vector3 targetPositionLastFrame;

    private Area currentlySelectedArea;
    private Area currentlyHoveredArea;

    public void Start()
    {
        SetBrushSize(BrushStartDiameter, false);

        // Set up game object with mesh;
        mf = gameObject.GetComponent<MeshFilter>();

        //add the newly generated mesh as the meshfilter
        BrushMesh b = Mesh2DFactory.CreateCircleMesh(1, AreaResolution);
        mf.mesh = b.GetUnityMesh();
    }

    public void Update()
    {
        LerpBrushSize();

        Area hoveredArea = areaManager.GetAreaAt(targetPosition);

        if (PlayerInput.Instance.IsTyping)
            return;

        //Keyboard Inputs
        if (hoveredArea)
        {
            if (Input.GetKeyDown(FillKeyCode))
            {
                hoveredArea.FillHoles();
                areaManager.SubtractFromAll(hoveredArea);
            }
            else if (Input.GetKey(ExpandKeyCode))
            {
                OnExpand(hoveredArea);
                //areaManager.SubtractFromAll(hoveredArea);
            }
            else if (Input.GetKey(ShrinkKeyCode))
            {
                OnShrink(hoveredArea);
            }
            else if (Input.GetKeyDown(DeleteKeyCode))
            {
                OnDelete(hoveredArea);
            }
        }


        if (deleting)
        {
            OnContinueSubtract();
        }
    }

    public void SetTargetPosition(Vector3 position)
    {
        targetPosition = position + new Vector3(0, Area.BrushToolPosition, 0);

        transform.position = targetPosition;
    }

    public void IncreaseBrushSize()
    {
        SetBrushSize(GetBrushSize() + scalingPerScroll * Time.deltaTime);
    }

    public void DecreaseBrushSize()
    {
        SetBrushSize(GetBrushSize() - scalingPerScroll * Time.deltaTime);
    }

    private void SetBrushSize(float size, bool lerp = true)
    {
        size = Mathf.Clamp(size, MinimumBrushDiameter, MaximumBrushDiameter);

        if (!lerp)
            transform.localScale = Vector3.one * size;

        targetDiameter = size;
    }

    private float GetBrushSize()
    {
        return targetDiameter;
    }

    private void LerpBrushSize()
    {
        Vector3 current = transform.localScale;
        Vector3 target = Vector3.one * targetDiameter;

        transform.localScale = Vector3.Lerp(current, target, Time.deltaTime * ScalingLerpSpeed);
    }

    //called on the frame left mouse button is clicked down
    private Area OnStartDraw()
    {
        //did we hit an existing area or a new area?
        Area a = areaManager.GetAreaAt(targetPosition);

        bool hitNewArea = a == null;
        if (hitNewArea)
        {
            return areaManager.StartNewArea(GetBrushSize(), TargetPosition2D(), AreaResolution);
        }
        //hit an already existing area
        else
        {
            //merge the two zones
            return a;
        }
    }

    //called on any frame left click is held
    private void OnContinueDraw(Area a)
    {
        if (a == null)
            return;

        Vector2 targetPositionLastFrameXZ = new Vector2(targetPositionLastFrame.x, targetPositionLastFrame.z);

        BrushMesh roundedLine = Mesh2DFactory.CreateRoundedLineStripMesh(GetBrushSize(),
            10, targetPositionLastFrameXZ, TargetPosition2D());

        //BrushMesh b = Mesh2DFactory.CreateCircleMesh(GetBrushSize(), AreaResolution, TargetPosition2D());
        a.Merge(roundedLine);

        //subtract from all areas
        areaManager.SubtractFromAll(a.GetBounds(), roundedLine, a);
        areaManager.ClipCurrentContext(a);
    }

    private void OnContinueSubtract()
    {
        BrushMesh mesh = Mesh2DFactory.CreateCircleMesh(GetBrushSize(), AreaResolution, TargetPosition2D());
        //subtract from all areas on this layer
        Bounds b = new Bounds(targetPosition, Vector3.one * (2 * GetBrushSize()));
        /*Vector3 bottomLeft = b.min;
        bottomLeft.y = b.center.y;
        Vector3 topRight = b.max;
        topRight.y = b.center.y;
        Vector3 bottomRight = new Vector3(b.max.x, b.center.y, b.min.z);
        Vector3 topLeft = new Vector3(b.min.x, b.center.y, b.max.z);
        
        Debug.DrawLine(bottomLeft, bottomRight, Color.red,Time.deltaTime, false);
        Debug.DrawLine(bottomLeft, topLeft, Color.red,Time.deltaTime, false);
        Debug.DrawLine(topLeft, topRight, Color.red,Time.deltaTime, false);
        Debug.DrawLine(topRight, bottomRight, Color.red,Time.deltaTime, false);*/
        areaManager.SubtractFromAll(b, mesh);
    }

    private void OnExpand(Area a)
    {
        a.Expand(ExpandFactor * Time.deltaTime);
        areaManager.SubtractFromAll(a);
        areaManager.ClipCurrentContext(a);
    }

    private void OnShrink(Area a)
    {
        a.Shrink(ExpandFactor * Time.deltaTime);
    }

    private void OnDelete(Area a)
    {
        areaManager.DeleteAreaAndChildren(a);
    }

    private Vector2 TargetPosition2D()
    {
        return new Vector2(targetPosition.x, targetPosition.z);
    }

    private bool deleting = false;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            currentlySelectedArea = OnStartDraw();
            targetPositionLastFrame = targetPosition;
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            deleting = true;
        }
    }

    public void OnScroll(PointerEventData eventData)
    {
        float scrollAxis = eventData.scrollDelta.y;

        if (scrollAxis > 0f) // forward
        {
            IncreaseBrushSize();
        }
        else if (scrollAxis < 0f) // backwards
        {
            DecreaseBrushSize();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            OnContinueDraw(currentlySelectedArea);
            targetPositionLastFrame = targetPosition;
        }
        /*else if (eventData.button == PointerEventData.InputButton.Right)
        {
            OnContinueSubtract();
        }*/
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            deleting = false;
        }
    }
}