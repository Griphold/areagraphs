using System.Collections.Generic;

public struct Edge
{
    public int P1, P2;

    public Edge(int P1, int P2)
    {
        this.P1 = P1;
        this.P2 = P2;
    }
}

public class EdgeEqualityComparer : IEqualityComparer<Edge>
{
    public bool Equals(Edge x, Edge y)
    {
        return (x.P1 == y.P1 && x.P2 == y.P2) || (x.P1 == y.P2 && x.P2 == y.P1);
    }

    public int GetHashCode(Edge obj)
    {
        unchecked
        {
            return obj.P1 < obj.P2 ? obj.P1 : obj.P2;
        }
    }
}