using System.Collections.Generic;
using System.Linq;

public class EdgeLoop : List<Edge>
{
    public List<int> AsIndexList()
    {
        List<int> indexList = new List<int>();
        for (int i = 0; i < this.Count; i++)
        {
            Edge edge = this[i];
            //add both indexes if this is the first edge
            if (i == 0)
            {
                indexList.Add(edge.P1);
                indexList.Add(edge.P2);
            }
            //add only P2 index as long as we are not at the last edge (avoid duplicated entry of first index)
            else if (i < this.Count - 1)
            {
                indexList.Add(edge.P2);
            }
        }

        return indexList;
    }

    public override string ToString()
    {
        return AsIndexList().Aggregate("EdgeLoop: ", (s, i) => { return s + i + ", "; });
    }
}