using UnityEngine;

public class HighlightOnHover : MonoBehaviour
{
    [SerializeField] private AreaManager areaManager;

    private Area currentlyHoveredArea = null;

    // Update is called once per frame
    void Update()
    {
        Area hoveredArea = areaManager.GetAreaAt(transform.position);
        Highlight(hoveredArea);
    }


    private void Highlight(Area a)
    {
        if (currentlyHoveredArea == null)
        {
            if (a)
            {
                a.SetHighlight(true);
                currentlyHoveredArea = a;
            }
            else
            {
                currentlyHoveredArea = null;
            }
        }
        else
        {
            if (a)
            {
                if (currentlyHoveredArea != a)
                {
                    currentlyHoveredArea.SetHighlight(false);
                    a.SetHighlight(true);
                    currentlyHoveredArea = a;
                }
            }
            else
            {
                currentlyHoveredArea.SetHighlight(false);
                currentlyHoveredArea = null;
            }
        }
    }
}