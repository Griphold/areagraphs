using System;
using System.Collections.Generic;
using UnityEngine;
using DoublePath = System.Collections.Generic.List<ClipperLib.DoublePoint>;
using IntPath = System.Collections.Generic.List<ClipperLib.IntPoint>;


public class Mesh2DFactory
{
    private const float VertexWeldTolerance = 0.25f;

    //Create 3D Circle Mesh with
    //- 3D representation for unity
    //- 2D representation for clipper library

    public static BrushMesh CreateCircleMesh(float radius, int resolution)
    {
        return CreateCircleMesh(radius, resolution, Vector2.zero);
    }

    public static BrushMesh CreateCircleMesh(float radius, int resolution, Vector2 center)
    {
        //calculate edge points with sine and cosine
        List<Vector2> edgePoints = new List<Vector2>();
        float degreesPerPoint = 2f * (float) Math.PI / (resolution - 1);
        for (int i = 0; i < resolution; i++)
        {
            float x = Mathf.Sin(degreesPerPoint * i) * radius + center.x;
            float y = Mathf.Cos(degreesPerPoint * i) * radius + center.y;
            edgePoints.Add(new Vector2(x, y));
        }

        //triangulate using Triangle.NET
        int[] indices = TriangulatorDelaunay.Triangulate(edgePoints);

        // Create the Vector3 vertices (2D -> 3D: x = x, y = z)
        Vector3[] vertices3D = new Vector3[edgePoints.Count];
        for (int i = 0; i < edgePoints.Count; i++)
        {
            vertices3D[i] = new Vector3(edgePoints[i].x, 0, edgePoints[i].y);
        }

        BrushMesh result = new BrushMesh(edgePoints, vertices3D, indices);

        return result;
    }

    public static BrushMesh CreateRoundedLineStripMesh(float radius, int resolutionPerEnd, Vector2 start, Vector2 end)
    {
        Vector2 direction = end - start;
        Vector2 normal = new Vector2(-direction.y, direction.x).normalized; //turn vector 90° anticlockwise;
        float angleOffsetFromUp = Mathf.Deg2Rad * Vector2.SignedAngle(normal, Vector2.up);

        //create rounded end at end
        List<Vector2> edgePoints = new List<Vector2>();
        float degreesPerPoint = (float) Math.PI / (resolutionPerEnd - 1); //180° divided by number of points - 1

        for (int i = 0; i < resolutionPerEnd; i++)
        {
            float angle = angleOffsetFromUp + degreesPerPoint * i;
            float x = Mathf.Sin(angle) * radius + end.x;
            float y = Mathf.Cos(angle) * radius + end.y;
            edgePoints.Add(new Vector2(x, y));
        }

        angleOffsetFromUp += 180 * Mathf.Deg2Rad;

        //create rounded end at start
        for (int i = 0; i < resolutionPerEnd; i++)
        {
            float angle = angleOffsetFromUp + degreesPerPoint * i;
            float x = Mathf.Sin(angle) * radius + start.x;
            float y = Mathf.Cos(angle) * radius + start.y;
            edgePoints.Add(new Vector2(x, y));
        }

        //triangulate using Triangle.NET
        int[] indices = TriangulatorDelaunay.Triangulate(edgePoints);

        // Create the Vector3 vertices (2D -> 3D: x = x, y = z)
        Vector3[] vertices3D = new Vector3[edgePoints.Count];
        for (int i = 0; i < edgePoints.Count; i++)
        {
            vertices3D[i] = new Vector3(edgePoints[i].x, 0, edgePoints[i].y);
        }

        //DrawDebugMesh(vertices3D, indices.ToList());
        //Debug.Log(indices.Max(x => x));
        //Debug.Log(edgePoints.Count);

        BrushMesh result = new BrushMesh(edgePoints, vertices3D, indices);

        return result;
    }

    public static Mesh CreateMeshFromEdgeVertices(Vector3[] vertexPositions, List<EdgeLoop> boundaryEdgeLoops,
        List<EdgeLoop> holeLoops)
    {
        List<List<Vector2>> boundaryEdgeLoops2D = new List<List<Vector2>>();
        List<List<Vector2>> holeLoops2D = new List<List<Vector2>>();

        //convert boundaries to 2D
        foreach (EdgeLoop edgeLoop in boundaryEdgeLoops)
        {
            List<Vector2> edgeLoop2D = new List<Vector2>();
            foreach (Edge edge in edgeLoop)
            {
                Vector3 positionP1 = vertexPositions[edge.P1];
                Vector3 positionP2 = vertexPositions[edge.P2];
                Vector2 p1Pos2D = new Vector2(positionP1.x, positionP1.z);
                Vector2 p2Pos2D = new Vector2(positionP2.x, positionP2.z);

                //"weld vertices that are closer than 0.5
                if (Vector2.SqrMagnitude(p2Pos2D - p1Pos2D) >= VertexWeldTolerance)
                    edgeLoop2D.Add(p1Pos2D);
            }

            if (edgeLoop2D.Count >= 3)
                boundaryEdgeLoops2D.Add(edgeLoop2D);
        }

        //convert holes to 2D
        if (holeLoops != null && holeLoops.Count >= 0)
        {
            foreach (EdgeLoop edgeLoop in holeLoops)
            {
                List<Vector2> edgeLoop2D = new List<Vector2>();
                foreach (Edge edge in edgeLoop)
                {
                    Vector3 positionP1 = vertexPositions[edge.P1];
                    Vector3 positionP2 = vertexPositions[edge.P2];
                    Vector2 p1Pos2D = new Vector2(positionP1.x, positionP1.z);
                    Vector2 p2Pos2D = new Vector2(positionP2.x, positionP2.z);

                    //"weld vertices that are closer than 0.5
                    if (Vector2.SqrMagnitude(p2Pos2D - p1Pos2D) >= VertexWeldTolerance)
                        edgeLoop2D.Add(p1Pos2D);
                }

                if (edgeLoop2D.Count >= 3)
                    holeLoops2D.Add(edgeLoop2D);
            }
        }

        //triangulate using triangle.net
        List<Vector3> vertices3D = new List<Vector3>();
        int[] indices = TriangulatorDelaunay.TriangulateWithHoles(boundaryEdgeLoops2D, holeLoops2D, out vertices3D);

        Mesh mesh = new Mesh();
        mesh.vertices = vertices3D.ToArray();
        mesh.triangles = indices;

        return mesh;
    }

    private static void DrawDebugMesh(Vector3[] vertices, List<int> triangles)
    {
        for (int i = 0; i < triangles.Count - 3; i += 3)
        {
            int index0 = triangles[i];
            int index1 = triangles[i + 1];
            int index2 = triangles[i + 2];

            if (index0 >= vertices.Length || index1 >= vertices.Length || index2 >= vertices.Length)
                continue;

            Vector3 v0 = vertices[index0];
            Vector3 v1 = vertices[index1];
            Vector3 v2 = vertices[index2];

            Debug.DrawLine(v0, v1, Color.magenta, 10f, false);
            Debug.DrawLine(v1, v2, Color.magenta, 10f, false);
            Debug.DrawLine(v2, v0, Color.magenta, 10f, false);
        }
    }
}