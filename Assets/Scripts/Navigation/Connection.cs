using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class Connection : MonoBehaviour, IPointerDownHandler
{
    private LineRenderer lineRenderer;
    
    private Area currentArea;
    private Connection otherConnection;

    private bool isDirect = false;
    private Connection explicitConnection;
    private List<Connection> implicitConnections = new List<Connection>();

    public Area CurrentArea
    {
        get => currentArea;
    }

    public Connection OtherConnection
    {
        get => otherConnection;
        set
        {
            otherConnection = value;
        } 
    }

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void OnDestroy()
    {
        if (isDirect && currentArea)
        {
            currentArea.RemoveConnection(this);
            foreach (Connection c in implicitConnections)
            {
                c.currentArea.RemoveConnection(c);
            }
            
        }
    }

    public void UpdateExplicitConnection(AreaManager areaManager)
    {
        //remove old association
        if(currentArea)
            currentArea.RemoveConnection(this);
        
        //get area at current transform position on plane
        currentArea = areaManager.GetAreaAt(transform.position, false);
        if(currentArea)
            currentArea.AddConnection(this);
        
        //update line renderer position
        if(otherConnection)
            lineRenderer.SetPosition(1, transform.InverseTransformPoint(otherConnection.transform.position));
    }

    public void UpdateImplicitConnections()
    {
        if (!isDirect)
        {
            Debug.LogWarning("Tried to update implicit connections in indirect connection.");
            return;
        }

        //delete old implicit connections
        foreach (Connection c in implicitConnections)
        {
            c.currentArea.RemoveConnection(c);
            Destroy(c);
        }
        
        foreach (Connection c in otherConnection.implicitConnections)
        {
            c.currentArea.RemoveConnection(c);
            Destroy(c);
        }
        
        //create implicit connections stack
        Stack<Connection> startConnections = this.CreateImplicitConnections();
        implicitConnections = startConnections.ToList();
        startConnections.Push(this);
        
        Stack<Connection> endConnections = otherConnection.CreateImplicitConnections();
        otherConnection.implicitConnections = endConnections.ToList();
        endConnections.Push(otherConnection);
                
        //fill up stack with copy of end of stack
        if (startConnections.Count > endConnections.Count)
        {
            Connection copy = endConnections.Peek();
            while (startConnections.Count > endConnections.Count)
            {
                endConnections.Push(copy);
            }
        }
        else if(startConnections.Count < endConnections.Count)
        {
            Connection copy = startConnections.Peek();
            while (startConnections.Count < endConnections.Count)
            {
                startConnections.Push(copy);
            }
        }
                
        //associate connections with each other
        while (startConnections.Count > 0)
        {
            Connection s = startConnections.Pop();
            Connection e = endConnections.Pop();

            s.OtherConnection = e;
            e.OtherConnection = s;
        }
    }

    public bool IsDirect
    {
        get => isDirect;
        set => isDirect = value;
    }

    private Stack<Connection> CreateImplicitConnections()
    {
        Stack<Connection> implicitStack = new Stack<Connection>();

        if (currentArea && otherConnection && otherConnection.currentArea )
        {
            List<Area> myBranch = currentArea.GetAncestors();
            myBranch.Add(currentArea);
            int myBranchLength = myBranch.Count;
            int otherBranchLength = otherConnection.currentArea.GetTreeDepth() + 1;

            int numOfImplicits = myBranchLength > otherBranchLength ? myBranchLength : otherBranchLength;
            numOfImplicits--;

            for (int i = 0; i < numOfImplicits; i++)
            {
                Connection c = gameObject.AddComponent<Connection>();
                int ancestorIndex = Mathf.Clamp(i,0, myBranchLength - 1);
                c.currentArea = myBranch[ancestorIndex];
                c.currentArea.AddConnection(c);
                c.explicitConnection = this;
                c.isDirect = false;
                
                implicitStack.Push(c);
            }
        }
        
        return implicitStack;
    }

    public void OnDrawGizmos()
    {
        if(!otherConnection)
            return;
        
        Gizmos.color = Color.black;
        Gizmos.DrawLine(transform.position, otherConnection.transform.position);
    }

    #region Serialization

    public class ConnectionSave
    {
        public float StartX, StartY, StartZ;
        public float EndX, EndY, EndZ;
    }

    public ConnectionSave GetSaveData()
    {
        Vector3 startPosition = transform.position;
        Vector3 endPosition = otherConnection.transform.position;
        
        ConnectionSave save = new ConnectionSave()
        {
            StartX = startPosition.x,
            StartY = startPosition.y,
            StartZ = startPosition.z,
            
            EndX = endPosition.x,
            EndY = endPosition.y,
            EndZ = endPosition.z
        };

        return save;
    }

    #endregion

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (isDirect)
            {
                
                Destroy(this.gameObject);
                Destroy(otherConnection.gameObject);
            }
        }
    }
}
