using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ConnectorTool : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private AreaManager areaManager;
    [SerializeField] private Connection IndicatorPrefab;

    private Transform myTransform;

    private Vector3 targetPosition;
    private Vector3 startPosition;
    private Area startArea;
    private bool connecting = false;
    private Connection currentStartConnection;
    private Connection currentEndConnection;

    private List<Connection> allConnectionStarts = new List<Connection>();

    private void Start()
    {
        myTransform = transform;
    }

    //Recalculate each connection when going into connect mode
    private void OnEnable()
    {
        ClearNullConnections();

        foreach (Connection c in allConnectionStarts)
        {
            c.UpdateExplicitConnection(areaManager);
            c.OtherConnection.UpdateExplicitConnection(areaManager);
            c.UpdateImplicitConnections();
        }

        //check if the connections are still valid (connecting two childless areas)
        foreach (Connection c in allConnectionStarts)
        {
            //if a connection is not connecting anything OR
            //if the connection is between two childless areas: delete it
            if (c.CurrentArea == null || c.OtherConnection.CurrentArea == null
                                      || !c.CurrentArea.IsChildless() || !c.OtherConnection.CurrentArea.IsChildless())
            {
                Destroy(c.gameObject);
                Destroy(c.OtherConnection.gameObject);
            }
        }

        ClearNullConnections();
    }

    private void OnDisable()
    {
        if (connecting)
            OnCancelConnect();
    }

    public void SetTargetPosition(Vector3 position)
    {
        targetPosition = position + new Vector3(0, Area.BrushToolPosition, 0);

        transform.position = targetPosition;

        if (connecting)
        {
            currentEndConnection.transform.position = targetPosition;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!enabled)
            return;

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (connecting)
            {
                OnEndConnect();
            }
            else
            {
                OnStartConnect();
            }
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (connecting)
            {
                OnCancelConnect();
            }
        }
    }

    private void OnStartConnect()
    {
        Area a = areaManager.GetAreaAt(myTransform.position, false);

        //Can only connect if starting from one area that is childless
        if (a && a.IsChildless())
        {
            //Debug.Log("start connecting");
            startArea = a;
            startPosition = myTransform.position;

            //make connection at start
            Connection startConnection = Instantiate(IndicatorPrefab);
            startConnection.transform.position = startPosition;

            //make connection at end (move this around until finalized)
            Connection endConnection = Instantiate(IndicatorPrefab);
            endConnection.transform.position = startPosition;

            //disable collider so it doesnt block the tool
            endConnection.GetComponent<Collider>().enabled = false;

            //mark as explicit connections
            startConnection.IsDirect = true;
            endConnection.IsDirect = true;

            //save connections for later editing in tool
            currentStartConnection = startConnection;
            currentEndConnection = endConnection;
            connecting = true;

            //add the connection to list of connections
            allConnectionStarts.Add(currentStartConnection);
        }
    }

    private void OnCancelConnect()
    {
        Destroy(currentStartConnection.gameObject);
        Destroy(currentEndConnection.gameObject);

        connecting = false;
    }

    private void OnEndConnect()
    {
        Area a = areaManager.GetAreaAt(transform.position, false);

        //Can only connect if ending at a different area
        if (a && a != startArea && a.IsChildless())
        {
            currentStartConnection.OtherConnection = currentEndConnection;
            currentEndConnection.OtherConnection = currentStartConnection;
            currentStartConnection.UpdateExplicitConnection(areaManager);
            currentEndConnection.UpdateExplicitConnection(areaManager);

            currentStartConnection.UpdateImplicitConnections();

            //enable collider 
            currentEndConnection.GetComponent<Collider>().enabled = true;

            currentStartConnection = null;
            currentEndConnection = null;
            connecting = false;
        }
    }

    public List<Connection.ConnectionSave> GetSaveData()
    {
        ClearNullConnections();

        List<Connection.ConnectionSave> result = new List<Connection.ConnectionSave>();
        foreach (Connection connection in allConnectionStarts)
        {
            if (connection.IsDirect)
                result.Add(connection.GetSaveData());
        }

        return result;
    }

    public void DeleteAllConnections()
    {
        foreach (Connection connection in allConnectionStarts)
        {
            if (connection)
            {
                Destroy(connection.OtherConnection.gameObject);
                Destroy(connection.gameObject);
            }
        }

        allConnectionStarts.Clear();
    }

    private void ClearNullConnections()
    {
        for (int i = 0; i < allConnectionStarts.Count; i++)
        {
            Connection connection = allConnectionStarts[i];
            if (!connection)
                allConnectionStarts.RemoveAt(i);
        }
    }

    public void InitializeFromSaveData(List<Connection.ConnectionSave> connectionSaves)
    {
        foreach (Connection.ConnectionSave save in connectionSaves)
        {
            Connection c = InitializeSingleConnectionFromSaveData(save);

            allConnectionStarts.Add(c);
        }
    }

    private Connection InitializeSingleConnectionFromSaveData(Connection.ConnectionSave save)
    {
        //make connection at start
        Connection startConnection = Instantiate(IndicatorPrefab);
        startConnection.transform.position = new Vector3(save.StartX, save.StartY, save.StartZ);
        startConnection.IsDirect = true;

        //make connection at end
        Connection endConnection = Instantiate(IndicatorPrefab);
        endConnection.transform.position = new Vector3(save.EndX, save.EndY, save.EndZ);
        endConnection.IsDirect = true;

        //let connections find their areas
        startConnection.OtherConnection = endConnection;
        endConnection.OtherConnection = startConnection;
        startConnection.UpdateExplicitConnection(areaManager);
        endConnection.UpdateExplicitConnection(areaManager);

        startConnection.UpdateImplicitConnections();

        return startConnection;
    }
}