using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathGenerator : MonoBehaviour
{
    [SerializeField] private AreaManager areaManager;
    [SerializeField] private ConnectorTool connectorTool;
    [SerializeField] private PathVisualizer pathVisualizer;
    [SerializeField] private Transform testStart;
    [SerializeField] private Transform testEnd;

    private PathVisualizer currentShownPath = null;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            Vector3 start = testStart.position;
            Vector3 end = testEnd.position;
            FindAndShowPaths(start, end);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            if (currentShownPath)
                Destroy(currentShownPath.gameObject);

            testStart.transform.position = connectorTool.transform.position;
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            if (currentShownPath)
                Destroy(currentShownPath.gameObject);

            testEnd.transform.position = connectorTool.transform.position;
        }
    }

    public void FindAndShowPaths(Vector3 start, Vector3 end)
    {
        //Find areas to connect
        Area startArea = areaManager.GetAreaAt(start, false);
        Area endArea = areaManager.GetAreaAt(end, false);

        //stop if we dont start and end in a valid area or theyre in the same area
        if (startArea == null || endArea == null || startArea == endArea)
            return;

        //make parent list of each area going from elder areas
        List<Area> startBranch = startArea.GetAncestors();
        startBranch.Add(startArea);
        List<Area> endBranch = endArea.GetAncestors();
        endBranch.Add(endArea);

        //make list of valid areas
        HashSet<Area> validAreas = new HashSet<Area>(areaManager.GetElderAreas());

        bool pathChanged = true;
        List<List<Area>> paths = new List<List<Area>>();

        //repeat until no new nodes are added
        while (pathChanged)
        {
            //choose start and end area
            Area startAreaThisRound = startBranch[0];
            Area endAreaThisRound = endBranch[0];

            //pathfind
            List<Area> pathFound = FindPath(startAreaThisRound, endAreaThisRound, validAreas);

            pathChanged = false;
            //check if path has changed
            if (paths.Count == 0)
            {
                pathChanged = true;
            }
            else
            {
                //check if path has changed
                List<Area> lastPath = paths[paths.Count - 1];
                pathChanged = !lastPath.SequenceEqual(pathFound);
            }

            if (pathChanged)
            {
                paths.Add(pathFound);

                //add children of path this round for pathfinding next round
                List<Area> children = GetAllChildren(pathFound);
                validAreas.Clear();
                foreach (List<Area> path in paths)
                {
                    foreach (Area areaOnPath in path)
                    {
                        validAreas.Add(areaOnPath);
                    }
                }

                foreach (Area childOnPath in children)
                {
                    if (!validAreas.Contains(childOnPath))
                    {
                        validAreas.Add(childOnPath);
                    }
                }
            }


            //remove highest branch node if possible
            if (startBranch.Count > 1)
                startBranch.RemoveAt(0);

            if (endBranch.Count > 1)
                endBranch.RemoveAt(0);
        }

        List<Color> colors = new List<Color>() {Color.red, Color.cyan, Color.magenta, Color.yellow, Color.green};

        //visualise paths along centers of bounding boxes
        //only show last path
        List<Area> finalPath = paths[paths.Count - 1];
        bool fullPathFound = finalPath[finalPath.Count - 1].Equals(endArea);
        VisualizePath(start, end, paths[paths.Count - 1], colors[0], fullPathFound);

        foreach (List<Area> path in paths)
        {
            ShowPathDebug(path);
        }
    }

    private List<Area> FindPath(Area startArea, Area endArea, HashSet<Area> validAreas)
    {
        AreaMap finalMap = new AreaMap();
        AreaMap temporaryMap = new AreaMap();
        temporaryMap.Add(startArea, new AreaNode() {PathLength = 0, Predecessor = null});
        Area frontierNode = null;

        //algorithm ends when the end's neighbors are about to be checked OR
        //there are no more nodes to explore
        while (frontierNode != endArea && temporaryMap.Count > 0)
        {
            //Get Lowest cost node
            frontierNode = temporaryMap.GetShortestArea();

            //remove from temp and add to final map
            AreaNode n = temporaryMap[frontierNode];
            temporaryMap.Remove(frontierNode);
            finalMap.Add(frontierNode, n);

            //get its neighbors and add the costs
            List<Area> newNodes = frontierNode.GetNeighbors(false);

            foreach (Area neighbor in newNodes)
            {
                //only visit area in a valid area, when specified
                if (validAreas.Count > 0 && !validAreas.Contains(neighbor))
                    continue;

                //do not visit area we have already visited
                if (finalMap.ContainsKey(neighbor))
                {
                    continue;
                }

                //calculate cost to the neighbor
                float cost = finalMap.GetPathLength(frontierNode) + HeuristicCost(frontierNode, neighbor);

                //check if that neighbor is already on the frontier, if so: update its cost if its lower
                if (temporaryMap.ContainsKey(neighbor))
                {
                    AreaNode existingEntry = temporaryMap[neighbor];

                    //check tree depth: prioritize predecessors with lower tree depth
                    int frontierNodeDepth = frontierNode.GetTreeDepth();
                    int existingEntryDepth = existingEntry.Predecessor.GetTreeDepth();
                    if (frontierNodeDepth > existingEntryDepth)
                    {
                        temporaryMap.Remove(neighbor);
                        temporaryMap.Add(neighbor, new AreaNode() {PathLength = cost, Predecessor = frontierNode});
                    }
                    else if (frontierNodeDepth == existingEntryDepth)
                    {
                        //if same tree depth take lower cost
                        if (cost < existingEntry.PathLength)
                        {
                            temporaryMap.Remove(neighbor);
                            temporaryMap.Add(neighbor, new AreaNode() {PathLength = cost, Predecessor = frontierNode});
                        }
                    }
                }
                //totally new node
                else
                {
                    temporaryMap.Add(neighbor, new AreaNode() {PathLength = cost, Predecessor = frontierNode});
                }
            }
        }

        //reconstruct path from end with predecessor entries
        List<Area> path = new List<Area>();
        Area currentArea = frontierNode;
        Area predecessor;

        while (currentArea != null)
        {
            predecessor = finalMap[currentArea].Predecessor;
            path.Add(currentArea);
            currentArea = predecessor;
        }

        //reverse list so path goes from start to end
        path.Reverse();
        return path;
    }

    private List<Area> GetAllChildren(List<Area> listOfParents)
    {
        List<Area> result = new List<Area>();

        foreach (Area areaOnPath in listOfParents)
        {
            List<Area> children = areaOnPath.GetChildren();
            if (children.Count > 0)
            {
                foreach (Area child in children)
                {
                    result.Add(child);
                }
            }
        }

        return result;
    }

    private void VisualizePath(Vector3 start, Vector3 end, List<Area> path, Color color, bool fullPathFound)
    {
        if (currentShownPath)
            Destroy(currentShownPath.gameObject);

        PathVisualizer v = Instantiate(pathVisualizer);
        v.SetColor(color);
        v.SetConnectEnd(fullPathFound);
        v.SetPath(start, end, path);

        currentShownPath = v;
    }

    private void ShowPathDebug(List<Area> path)
    {
        string debugText = "Path: ";
        for (int i = 0; i < path.Count - 1; i++)
        {
            Area a = path[i];
            //Debug.DrawLine(a.GetBounds().center, path[i+1].GetBounds().center, color, 10, false);
            debugText += a.name + " - ";

            if (i == path.Count - 2)
            {
                debugText += path[i + 1].name;
            }
        }

        Debug.Log(debugText);
    }

    private float HeuristicCost(Area a, Area b)
    {
        //heuristic is distance in bounding box centers
        float nodeToNodeDistance = Vector3.Distance(a.GetBounds().center, b.GetBounds().center);
        //multiply by depth change when going up the tree to search there last
        float depthChange = a.GetTreeDepth() - b.GetTreeDepth();
        float depthFactor = depthChange * 10000;

        return nodeToNodeDistance;
    }

    private struct AreaNode
    {
        public float PathLength { get; set; }

        public Area Predecessor;
    }

    private class AreaMap : Dictionary<Area, AreaNode>
    {
        public Area GetShortestArea()
        {
            Area currentShortestArea = null;
            float pathLength = Mathf.Infinity;
            foreach (Area area in this.Keys)
            {
                AreaNode node = this[area];
                if (node.PathLength < pathLength)
                {
                    pathLength = node.PathLength;
                    currentShortestArea = area;
                }
            }

            return currentShortestArea;
        }

        public float GetPathLength(Area area)
        {
            return this[area].PathLength;
        }
    }
}