using System.Collections.Generic;
using UnityEngine;

public class PathVisualizer : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private bool connectEnd;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void SetColor(Color c)
    {
        lineRenderer.material.color = c;
    }

    public void SetConnectEnd(bool connectEnd)
    {
        this.connectEnd = connectEnd;
    }

    public void SetPath(Vector3 start, Vector3 end, List<Area> path)
    {
        List<Vector3> points = new List<Vector3>(path.Count + 2);

        //add start, then path points, then end
        start.y = Area.PathYPosition;
        points.Add(start);
        foreach (Area area in path)
        {
            Vector3 areaBoundsCenter = area.GetBounds().center;
            areaBoundsCenter.y = Area.PathYPosition;
            points.Add(areaBoundsCenter);
        }

        if (connectEnd)
        {
            end.y = Area.PathYPosition;
            points.Add(end);
        }

        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPositions(points.ToArray());
    }
}