using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private DrawTool drawTool;
    [SerializeField] private ConnectorTool connector;

    public static PlayerInput Instance { get; set; }

    private bool isTyping = false;

    public bool IsTyping
    {
        get => isTyping;
        set => isTyping = value;
    }

    private void Awake()
    {
        Instance = this;
        EnableDrawMode(); //default to draw mode at start
    }

    /*private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            isTyping = false;
    }*/

    public void EnableDrawMode()
    {
        drawTool.gameObject.SetActive(true);
        connector.gameObject.SetActive(false);
    }

    public void EnableConnectMode()
    {
        drawTool.gameObject.SetActive(false);
        connector.gameObject.SetActive(true);
    }

    public void SetTypingToTrue()
    {
        isTyping = true;
    }

    public void SetTypingToFalse()
    {
        isTyping = false;
    }
}