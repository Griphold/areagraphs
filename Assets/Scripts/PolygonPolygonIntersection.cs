using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;
using Path = System.Collections.Generic.List<ClipperLib.DoublePoint>;
using Paths = System.Collections.Generic.List<System.Collections.Generic.List<ClipperLib.DoublePoint>>;

public class PolygonPolygonIntersection : MonoBehaviour
{

    private void OnDrawGizmos()
    {
        Path circlePoints = CreateCirclePoints(new DoublePoint(200, 200), 40, 40);
        DrawGeometry(Color.cyan, circlePoints);

        Path circlePoints2 = CreateCirclePoints(new DoublePoint(180, 180), 25, 40);
        DrawGeometry(Color.cyan, circlePoints2);

        Paths subject = new Paths(1);
        subject.Add(circlePoints);

        Paths clip = new Paths(1);
        clip.Add(circlePoints2);
        /*Paths subj = new Paths(2);
        subj.Add (new Path(4));
        subj[0].Add(new IntPoint(180, 200));
        subj[0].Add(new IntPoint(180, 150));
        subj[0].Add(new IntPoint(260, 150));
        subj[0].Add(new IntPoint(260, 200));	

        subj.Add(new Path(3));
        subj[1].Add(new IntPoint(215, 160));
        subj[1].Add(new IntPoint(230, 190));	
        subj[1].Add(new IntPoint(200, 190));

        Paths clip = new Paths(1);
        clip.Add(new Path(4));
        clip[0].Add(new IntPoint(190, 210));
        clip[0].Add(new IntPoint(240, 210));	
        clip[0].Add(new IntPoint(240, 130));
        clip[0].Add(new IntPoint(190, 130));
        
        DrawGeometry(Color.red, subj);
        DrawGeometry(Color.cyan, clip);

        Paths solution = new Paths();
*/
        //TODO scale input and output
        Paths union = Clip(subject, clip, ClipType.ctUnion);
        Paths intersection = Clip(subject, clip, ClipType.ctIntersection);
        //c.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

        DrawGeometries(Color.green, union, new Vector3(100, 0, 0));
        DrawGeometries(Color.red, intersection, new Vector3(200, 0, 0));
    }

    private void DrawGeometry(Color c, Path geometry)
    {
        Gizmos.color = c;
        for (int i = 0; i < geometry.Count; i++)
        {
            DoublePoint from = geometry[i];
            DoublePoint to = geometry[(i + 1) % (geometry.Count)];
            Gizmos.DrawLine(new Vector3((float) from.X, (float) from.Y, 0), new Vector3((float) to.X, (float) to.Y, 0));
        }
    }

    private void DrawGeometries(Color c, Paths geometry, Vector3 offset)
    {
        Gizmos.color = c;
        foreach (Path points in geometry)
        {
            for (int i = 0; i < points.Count; i++)
            {
                DoublePoint from = points[i];

                DoublePoint to = points[(i + 1) % (points.Count)];
                Gizmos.DrawLine(new Vector3((float) from.X, (float) from.Y, 0) + offset,
                    new Vector3((float) to.X, (float) to.Y, 0) + offset);
            }
        }
    }

    private Path CreateCirclePoints(DoublePoint center, float radius, int resolution)
    {
        Path circle = new Path(resolution);
        float degreesPerPoint = 2f * (float) Math.PI / (resolution - 1);
        for (int i = 0; i < resolution; i++)
        {
            double x = Mathf.Sin(degreesPerPoint * i) * radius + center.X;
            double y = Mathf.Cos(degreesPerPoint * i) * radius + center.Y;
            circle.Add(new DoublePoint(x, y));
        }

        return circle;
    }

    private Paths Clip(Paths paths, Paths clip, ClipType type)
    {
        Clipper c = new Clipper();

        //add original polygons
        foreach (Path path in paths)
        {
            List<IntPoint> converted = ConvertToIntList(path);
            c.AddPath(converted, PolyType.ptSubject, true);
        }

        //add clip
        foreach (Path path in clip)
        {
            List<IntPoint> converted = ConvertToIntList(path);
            c.AddPath(converted, PolyType.ptClip, true);
        }

        //produce solution
        List<List<IntPoint>> solution = new List<List<IntPoint>>();
        c.Execute(type, solution);

        //c.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

        //convert back to doubleList
        List<List<DoublePoint>> solutionPaths = new Paths();
        foreach (List<IntPoint> intPoints in solution)
        {
            List<DoublePoint> doublePoints = ConvertToDoubleList(intPoints);
            solutionPaths.Add(doublePoints);
        }

        return solutionPaths;
    }

    private List<IntPoint> ConvertToIntList(List<DoublePoint> doublePoints)
    {
        List<IntPoint> intPoints = new List<IntPoint>();

        foreach (DoublePoint doublePoint in doublePoints)
        {
            int scaledUpValueX = (int) (doublePoint.X * 1000d);
            int scaledUpValueY = (int) (doublePoint.Y * 1000d);

            intPoints.Add(new IntPoint(scaledUpValueX, scaledUpValueY));
        }

        return intPoints;
    }

    private List<DoublePoint> ConvertToDoubleList(List<IntPoint> intPoints)
    {
        List<DoublePoint> doublePoints = new List<DoublePoint>();

        foreach (IntPoint intPoint in intPoints)
        {
            double scaledDownValueX = intPoint.X / 1000d;
            double scaledDownValueY = intPoint.Y / 1000d;

            doublePoints.Add(new DoublePoint(scaledDownValueX, scaledDownValueY));
        }

        return doublePoints;
    }
}