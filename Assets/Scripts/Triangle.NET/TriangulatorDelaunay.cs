using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TriangleNet;
using TriangleNet.Geometry;
using TriangleNet.Meshing;
using TriangleNet.Topology;
using UnityEngine;

public class TriangulatorDelaunay
{
    /// <summary>
    /// Triangulates a closed edge loop
    /// </summary>
    /// <param name="points">List of edge loops in 2D XY-Space</param>
    public static int[] Triangulate(List<Vector2> points)
    {
        Polygon result = new Polygon();
        //create and add vertices
        List<Vertex> vertices = new List<Vertex>();
        foreach (Vector2 point in points)
        {
            vertices.Add(new Vertex(point.x, point.y));
        }
        
        for (int i = 0; i < vertices.Count; i++)
        {
            Vertex v0 = vertices[i];
            Vertex v1 = vertices[(i + 1) % vertices.Count];
            
            //add vertex
            result.Add(v0);
            
            //add segment
            result.Add(new Segment(v0, v1));
        }
        
        //options: Concave shape, Delaunay, do not create new vertices
        ConstraintOptions options = new ConstraintOptions();
        options.Convex = false;
        options.ConformingDelaunay = true;
        options.SegmentSplitting = 2;

        IMesh mesh = result.Triangulate(options);

        List<int> indices = new List<int>();
        foreach (Triangle triangle in mesh.Triangles)
        {
            indices.Add(triangle.vertices[0].ID);
            indices.Add(triangle.vertices[1].ID);
            indices.Add(triangle.vertices[2].ID);
        }
        indices.Reverse();
        
        //replace vertices with ID = last with ID = 0
        //for some reason any triangle using the 0th vertex duplicates that vertex
        //so we replace it with 0
        for (int i = 0; i < indices.Count; i++)
        {
            if (indices[i] == points.Count)
                indices[i] = 0;
        }
        
        return indices.ToArray();
    }

    public static int[] TriangulateWithHoles(List<List<Vector2>> edgeLoops, List<List<Vector2>> holesList, out List<Vector3> vertexPositions)
    {
        int vertsIn = 0;
        Polygon result = new Polygon();
        //create and add vertices
        foreach (List<Vector2> edgeLoop in edgeLoops)
        {
            List<Vertex> loopVertices = new List<Vertex>();
            for (int i = 0; i < edgeLoop.Count; i++)
            {
                Vector2 point = edgeLoop[i];
                Vertex vertex = new Vertex(point.x, point.y);
                loopVertices.Add(vertex);
                //result.Add(vertex);
                vertsIn++;
            }
            result.Add(new Contour(loopVertices));
        }

        //add holes
        foreach (List<Vector2> hole in holesList)
        {
            List<Vertex> holeVertices = new List<Vertex>();
            foreach (Vector2 point in hole)
            {
                holeVertices.Add(new Vertex(point.x, point.y));
                vertsIn++;
            }
            result.Add(new Contour(holeVertices), true);
        }
        
        //options: Concave shape, Delaunay, do not create new vertices
        ConstraintOptions options = new ConstraintOptions();
        options.Convex = false;
        options.ConformingDelaunay = true;
        options.SegmentSplitting = 2;
        

        QualityOptions qualityOptions = new QualityOptions();
        qualityOptions.UserTest = (triangle, d) =>
        {
            return false;
        };

        IMesh mesh = result.Triangulate(options, qualityOptions);
        
        List<int> indices = new List<int>();
        foreach (Triangle triangle in mesh.Triangles)
        {
            indices.Add(triangle.vertices[0].ID);
            indices.Add(triangle.vertices[1].ID);
            indices.Add(triangle.vertices[2].ID);
        }
        indices.Reverse();
        
        /*
        int highestID = 0;
        foreach (int index in indices)
        {
            if (index > highestID)
                highestID = index;
        }*/
        
        //replace vertices with ID = last with ID = 0
        //for some reason any triangle using the 0th vertex duplicates that vertex
        //so we replace it with 0
        /*for (int i = 0; i < indices.Count; i++)
        {
            if (indices[i] == highestID)
                indices[i] = 0;
        }*/

        //convert vertices to vector3
        mesh.Renumber();
        ICollection<Vertex> vertexPositionsPolygon = mesh.Vertices;
        vertexPositions = vertexPositionsPolygon.ToList().ConvertAll(vertex => new Vector3((float)vertex.x, 0, (float)vertex.y));
        
        //find out of bounds triangles
        List<int> outOfBoundsInts = new List<int>();
        foreach (int index in indices)
        {
            if (index >= vertexPositions.Count)
            {
                outOfBoundsInts.Add(index);
            }
        }

        String s = outOfBoundsInts.Aggregate("", (s1, i) => s1 + i + ", ");
        
        if(outOfBoundsInts.Count > 0)
            Debug.Log("maxindex: " + mesh.Vertices.Count + " | outOfBOunds: " + s);
        
        //Debug.Log("vertsIn: " + vertsIn + " | vertsOut: " + highestID);
        return indices.ToArray();
    }
}
