using System.Collections.Generic;
using System.Linq;
using csDelaunay;
using UnityEngine;

public class VoronoiTest : MonoBehaviour
{
    //Resolution
    public int Resolution = 1024;

    //List of all points
    public List<Transform> points = new List<Transform>();

    // This is where we will store the resulting data
    private Dictionary<Vector2f, Site> sites;
    private List<csDelaunay.Edge> edges;
    private Texture2D tx;
    private Rectf bounds;
    private Color32[] resetColorArray;

    void Start()
    {
        //Find AreaIndicators in Scene
        GameObject[] areas = GameObject.FindGameObjectsWithTag("Area");
        //add each gameobjects transform to our points to track
        points.AddRange(areas.Select(x => x.transform));

        // Create the bounds of the voronoi diagram
        // Use Rectf instead of Rect; it's a struct just like Rect and does pretty much the same,
        // but like that it allows you to run the delaunay library outside of unity (which mean also in another tread)
        bounds = new Rectf(0, 0, Resolution, Resolution);

        //Set up texture
        tx = new Texture2D(Resolution, Resolution);
        GetComponent<Renderer>().material.mainTexture = tx;

        // Reset all pixels color to transparent
        Color32 resetColor = new Color32(255, 255, 255, 255);
        resetColorArray = new Color32[Resolution * Resolution];

        for (int i = 0; i < resetColorArray.Length; i++)
        {
            resetColorArray[i] = resetColor;
        }
    }

    public void Update()
    {
        // Create your sites (lets call that the center of your polygons)
        List<Vector2f> points = ConvertPointList();

        // Create Voronoi diagram
        Voronoi voronoi = new Voronoi(points, bounds, 0);

        // Now retrieve the edges from it
        sites = voronoi.SitesIndexedByLocation;
        edges = voronoi.Edges;
        List<Circle> list = voronoi.Circles();

        DisplayVoronoiDiagram();
    }

    private List<Vector2f> ConvertPointList()
    {
        List<Vector2f> result = new List<Vector2f>();
        foreach (Transform t in points)
        {
            Vector3 pos = t.position;
            result.Add(new Vector2f(pos.x, pos.z));
        }

        return result;
    }

    // Here is a very simple way to display the result using a simple bresenham line algorithm
    // Just attach this script to a quad
    private void DisplayVoronoiDiagram()
    {
        //Reset everything to white
        tx.SetPixels32(resetColorArray);

        foreach (KeyValuePair<Vector2f, Site> kv in sites)
        {
            DrawCircle(tx, Color.red, (int) kv.Key.x, (int) kv.Key.y, 10);
        }

        foreach (csDelaunay.Edge edge in edges)
        {
            // if the edge doesn't have clippedEnds, if was not within the bounds, dont draw it
            if (edge.ClippedEnds == null) continue;

            DrawLine(edge.ClippedEnds[LR.LEFT], edge.ClippedEnds[LR.RIGHT], tx, Color.black);
        }

        tx.Apply();
    }

    // Bresenham line algorithm
    private void DrawLine(Vector2f p0, Vector2f p1, Texture2D tx, Color c, int offset = 0)
    {
        int x0 = (int) p0.x;
        int y0 = (int) p0.y;
        int x1 = (int) p1.x;
        int y1 = (int) p1.y;

        int dx = Mathf.Abs(x1 - x0);
        int dy = Mathf.Abs(y1 - y0);
        int sx = x0 < x1 ? 1 : -1;
        int sy = y0 < y1 ? 1 : -1;
        int err = dx - dy;

        while (true)
        {
            tx.SetPixel(x0 + offset, y0 + offset, c);

            if (x0 == x1 && y0 == y1) break;
            int e2 = 2 * err;
            if (e2 > -dy)
            {
                err -= dy;
                x0 += sx;
            }

            if (e2 < dx)
            {
                err += dx;
                y0 += sy;
            }
        }
    }

    //https://stackoverflow.com/questions/30410317/how-to-draw-circle-on-texture-in-unity
    private void DrawCircle(Texture2D tex, Color color, int x, int y, int radius = 3)
    {
        float rSquared = radius * radius;

        for (int u = x - radius; u < x + radius + 1; u++)
        for (int v = y - radius; v < y + radius + 1; v++)
            if ((x - u) * (x - u) + (y - v) * (y - v) < rSquared)
                tex.SetPixel(u, v, color);
    }
}