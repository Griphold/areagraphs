using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface GraphSerialisable
{
    Area.AreaSave GetSaveData();
}
