using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using YamlDotNet;
using UnityEngine;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

public class SerialisationManager : MonoBehaviour
{
    [SerializeField]
    private string savePath = "Assets/Saves";
    [SerializeField]
    private AreaManager areaManager;
    [SerializeField] private ConnectorTool connectionManager;
    [SerializeField]
    private KeyCode SaveKeyCode = KeyCode.F5; //F5 to save
    [SerializeField]
    private KeyCode LoadKeyCode = KeyCode.F6; //F5 to save

    private string saveName = "";
    
    public void Update()
    {
        
        //saving
        if (Input.GetKeyDown(SaveKeyCode))
        {
            if(saveName == "")
                return;
            
            Save("test");
        }
        //loading
        else if (Input.GetKeyDown(LoadKeyCode))
        {
            if(saveName == "")
                return;
            
            Load("test");
        }
    }

    public void SetSaveName(string name)
    {
        saveName = name;
    }

    public void OnSave()
    {
        Save(saveName);
    }

    public void OnLoad()
    {
        Load(saveName);
    }

    private void Save(string name)
    {
        string filePath = savePath + "/" + name;
        
        //check if save already exists and make sure to ask for overwrite
        if (Directory.Exists(savePath))
        {
            //ask overwrite
            bool overwrite = true;

            if (overwrite)
            {
                //delete all folders and files
            }
            else
            {
                return;
            }
        }
        
        
        //gather area save data
        List<Area.AreaSave> saveData = areaManager.GetSaveData();
        
        //create top folder
        if (!Directory.Exists(filePath))
        {
            DirectoryInfo folder = Directory.CreateDirectory(filePath);
        }

        //create mesh folder
        string meshPath = filePath + "/meshes";
        if (!Directory.Exists(meshPath))
        {
            DirectoryInfo meshFolder = Directory.CreateDirectory(meshPath);
        }
        
        //save meshes with IDs
        List<Area.AreaSave> allSavesLinear = AggregateDataRecursive(saveData);

        if (Directory.Exists(meshPath))
        {
            string[] GUIDs = AssetDatabase.FindAssets("t:Mesh", new[] {meshPath});
            string[] paths = GUIDs.Select(g => AssetDatabase.GUIDToAssetPath(g)).ToArray();

            List<string> failedPaths = new List<string>();
            AssetDatabase.DeleteAssets(paths, failedPaths);
        }

        //save tree from elder areas onward
        foreach (Area.AreaSave save in allSavesLinear)
        {
            string savePath = meshPath + "/" + save.ID + ".asset";
            Mesh mesh = save.mesh;
            AssetDatabase.CreateAsset(mesh, savePath);
        }
        
        //gather connection data
        List<Connection.ConnectionSave> connectionSaves = connectionManager.GetSaveData();
        
        //save text into text file
        AllData data = new AllData()
        {
            AreaSaves = saveData,
            ConnectionSaves = connectionSaves
        };
        
        ISerializer serializer = new SerializerBuilder().Build();
        string yaml = serializer.Serialize(data);

        StreamWriter sr = File.CreateText(filePath +"/" + name + ".yaml");
        sr.Write(yaml);
        sr.Flush();
        sr.Close();
    }

    private List<Area.AreaSave> AggregateDataRecursive(List<Area.AreaSave> areaSaves)
    {
        List<Area.AreaSave> allSaves = new List<Area.AreaSave>();

        allSaves.AddRange(areaSaves);
        
        foreach (Area.AreaSave save in areaSaves)
        {
            if(save.children.Length > 0)
                allSaves.AddRange(AggregateDataRecursive(save.children.ToList()));
        }

        return allSaves;
    }

    private void Load(string name)
    {
        string filePath = savePath + "/" + name;
        
        //find text file
        //if file exists load string from file
        string inputText;

        try
        {
            inputText =  System.IO.File.ReadAllText(filePath + "/" + name + ".yaml");
        }
        catch (FileNotFoundException fileNotFoundException)
        {
            Debug.LogError(fileNotFoundException.Message);
            return;
        }

        //deserialise text file to readable save data

        IDeserializer deserializer = new DeserializerBuilder().Build();

        AllData data = deserializer.Deserialize<AllData>(inputText);

        List<Area.AreaSave> areaSaves = data.AreaSaves;

        //create areas from meshes
        string[] GUIDs = AssetDatabase.FindAssets("t:Mesh", new[] {filePath + "/meshes"});

        Dictionary<string, Mesh> allAreaMeshes = new Dictionary<string, Mesh>();
        foreach (string guid in GUIDs)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            Mesh mesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);
            
            allAreaMeshes.Add(mesh.name, mesh);
        }
        
        //construct hierarchy of children and set correct context
        areaManager.InitializeFromSaveData(areaSaves, allAreaMeshes);
        
        //construct connections
        connectionManager.DeleteAllConnections();
        connectionManager.InitializeFromSaveData(data.ConnectionSaves);
    }

    public class AllData
    {
        public List<Area.AreaSave> AreaSaves;
        public List<Connection.ConnectionSave> ConnectionSaves;
    }
}
